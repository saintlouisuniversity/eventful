slu_eventful

This module connects to the eventful.com api for connecting to local events. The app includes configuration for categories, search queries, appkey, location, distance and days in future. See http://http://api.eventful.com/tools/tutorials/search for information on configuring this module's option.

Includes the required logo and link per the eventful terms of service.
=================