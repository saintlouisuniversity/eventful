<?php

class SluEventfulDataObject extends KGODataObject
{
    const TITLE_ATTRIBUTE = 'slueventful:event';
    const URL_ATTRIBUTE = 'slueventful:url';
    const VENUE_ATTRIBUTE = 'slueventful:venue_name';
    const DATE_ATTRIBUTE = 'slueventful:start_time';
    const DESC_ATTRIBUTE = 'slueventful:description';
    const IMG_ATTRIBUTE = 'slueventful:image';

    public function getTitle() {
        return $this->getAttribute(self::TITLE_ATTRIBUTE);
    }

    public function getEvtURL() {
        return $this->getAttribute(self::URL_ATTRIBUTE);
    }

    public function getVenue() {
        return $this->getAttribute(self::VENUE_ATTRIBUTE);
    }

    public function getEventDate() {
        return $this->getAttribute(self::DATE_ATTRIBUTE);
    }

    public function getDesc() {
        return $this->getAttribute(self::DESC_ATTRIBUTE);
    }
    public function getImg() {
        return $this->getAttribute(self::IMG_ATTRIBUTE);
    }

    public function getUIField(KGOUIObject $object, $field) {
        switch ($field) {
            case 'title':
                return $this->getTitle();
                break;
           case 'label':
           		$evDate = explode("-", $this->getEventDate());
           		$evTime = explode(" ", $evDate[2]);
           		$evHMS = explode(":",$evTime[1]);
           		if($evHMS[0]>12){$ampm = "pm";$h = $evHMS[0] - 12;$m = $evHMS[1];}
           		else{$ampm = "am";$h = $evHMS[0];$m = $evHMS[1];}
                return $this->getVenue() . " " . $evDate[1] ."/".$evTime[0]."/".$evDate[0]." ".$h.":".$m." ".$ampm;
                break;
             case 'subtitle':
                $sub =  $this->getDesc();
                return $sub;
                break;
            case 'thumbnail':
                $img = $this->getImg();
                $args = array('maxwidth'=>120);
                $proc = KGODataProcessor::factory("KGOImageDataProcessor", $args);
                return $proc->process($img);
                break;
            case 'url':
                return KGOURL::createForExternalURL($this->getEvtURL());
                break;
           default:
                return parent::getUIField($object, $field);
        }
    }
}
