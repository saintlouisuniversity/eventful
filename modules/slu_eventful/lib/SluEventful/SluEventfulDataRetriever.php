<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

class SluEventfulDataRetriever extends KGOURLDataRetriever {
    public static $slueventfulURLTemplate = "http://api.eventful.com/rest/events/search?c=%s&q=%s&app_key=%s&date=future&t=Next+%s+Days&sort_order=date&l=%s&within=%s";
    protected function init($args) {
        parent::init($args);

        $this->setBaseURL(sprintf(self::$slueventfulURLTemplate, $args['category'], $args['keyWord'], $args['appKey'], $args['days'], $args['zipCode'], $args['within']));
    }
}
